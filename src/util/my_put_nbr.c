/*
** my_put_nbr.c for  in /home/toruser/Piscine/C5/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 20 09:08:08 2017 LUGARD OVILA JULIEN
** Last update Mon Oct 23 12:24:37 2017 LUGARD OVILA JULIEN
*/
#include "util.h"

void	my_put_nbr(int nb)
{
  if (nb < 0)
    {
      if (nb > -10)
	{
	  my_putchar('-');
	  my_putchar(nb * (-1) + '0');
	  return ;
	}
      my_putchar('-');
      my_put_nbr((nb / 10) * (-1));
      my_putchar(((nb % 10) * (-1)) + '0');
    }
  else
    {
      if (nb >= 10)
	my_put_nbr(nb / 10);
      my_putchar((nb % 10) + '0');
    }
}
