/*
** util.h for  in /home/toruser/Piscine/SCHIFUMI/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 27 09:20:58 2017 LUGARD OVILA JULIEN
** Last update Sat Oct 28 11:51:13 2017 LUGARD OVILA JULIEN
*/
#ifndef		_UTIL_H_
# define	_UTIL_H_

#include <stdlib.h>
#include <unistd.h>

typedef			struct s_color	t_color;

struct			s_color
{
  char			*color;
  char			*unicode;
};

static const t_color	g_color[] =
  {
    {"clear", "\033[H\033[2J"},
    {"red", "\033[31m"},
    {"green", "\033[32m"},
    {"yellow", "\033[33m"},
    {"blue", "\033[34m"},
    {"magenta", "\033[35m"},
    {"cyan", "\033[36m"},
    {"reset_color","\033[0m"},
    {NULL, NULL}
  };

char  *my_strcat(char *dest, char *src);

void  my_cls(void);

int		my_strcmp(char *d, char *s);

int		my_strlen(char *s);

int		is_nbr(char *s);

void	my_putchar(char c);

void	my_putstr(char *s);

int		my_getnbr(char *str);

void	my_put_nbr(int nb);

char* my_strtok(char *str, const char* delim);

char	*readLine();

char	*my_strdup(const char *str);

void	my_putstr_color(const char *color, const char *str);

void  *my_memset(void *str, int c, size_t n);

void  my_perror(char *str);

int   my_exit(char *str);

char  *my_strcpy(char *dest, char *src);

char  *m_strdup(char *str);

int   my_isspace(char *s);

char *my_strstr(char *str, char *to_find);

void	my_swap(int *a, int *b);

char	*my_strncat(char *dest, char *src, int n);

int	my_strncmp(char *d, char *s, int n);

char *my_strncpy(char *dest, char *src, int n);



#endif		/* !_UTIL_H_ */
