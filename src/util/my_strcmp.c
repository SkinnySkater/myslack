/*
** my_strcmp.c for  in /home/toruser/Piscine/C4/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Thu Oct 19 12:11:17 2017 LUGARD OVILA JULIEN
** Last update Thu Oct 19 23:32:19 2017 LUGARD OVILA JULIEN
*/
#include "util.h"

int	my_strcmp (char *d, char *s)
{
  while (*d && *s && *d == *s)
    {
      d++;
      s++;
    }
  if ((*d - *s) < 0)
    return (-1);
  if ((*d - *s) > 0)
    return (1);
  else
    return (0);
}
