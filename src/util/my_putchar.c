/*
** my_putchar.c for  in /home/toruser/Piscine/C1/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Mon Oct 16 15:25:49 2017 LUGARD OVILA JULIEN
** Last update Mon Oct 16 21:53:17 2017 LUGARD OVILA JULIEN
*/

#include <unistd.h>
#include "util.h"

void	my_putchar(char c)
{
  write(STDOUT_FILENO, &c, sizeof(c));
}
