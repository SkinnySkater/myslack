/*
** readline.c for  in /home/toruser/Piscine/SCHIFUMI/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Fri Oct 27 13:14:26 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 13:15:19 2017 LUGARD OVILA JULIEN
*/

#include "util.h"

#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>

char		*readLine()
{
  ssize_t	ret;
  char		*buff;

  if ((buff = malloc(sizeof(*buff) * (1024 + 1))) == NULL)
    return (NULL);
  if ((ret = read(0, buff, 1025)) > 1)
    {
      buff[ret - 1] = '\0';
      return (buff);
    }
  buff[0] = '\0';
  return (buff);
}
