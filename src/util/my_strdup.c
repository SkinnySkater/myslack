/*
** my_strdup.c for  in /home/toruser/Piscine/C6/lugard_o
** 
** Made by LUGARD OVILA JULIEN
** Login   <lugard_o@etna-alternance.net>
** 
** Started on  Mon Oct 23 11:20:42 2017 LUGARD OVILA JULIEN
** Last update Fri Oct 27 08:56:28 2017 LUGARD OVILA JULIEN
*/
#include <stdlib.h>

int	m_strlen(char *str)
{
  int	i;

  i = 0;
  while (*str)
    {
      str++;
      i++;
    }
  return (i);
}

char	*m_strcpy(char *dest, char *src)
{
  int	i;

  i = 0;
  while (i < m_strlen(src))
    {
      dest[i] = src[i];
      i++;
    }
  dest[i] = '\0';
  return (dest);
}

char	*m_strdup(char *str)
{
  char	*dest;

  if (str == NULL)
    return (str);
  dest = malloc(sizeof(*dest) * (m_strlen(str) + 1));
  if (dest == NULL)
    return (dest);
  m_strcpy(dest, str);
  return (dest);
}
