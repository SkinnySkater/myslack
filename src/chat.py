# -*- coding: utf-8 -*-
from Tkinter import *
import tkMessageBox

def messageFilter(messageText):
    EndFiltered = ''
    for i in range(len(messageText)-1, -1, -1):
        if messageText[i] != '\n':
            EndFiltered = messageText[0:i + 1]
            break
    for i in range(0,len(EndFiltered), 1):
            if EndFiltered[i] != "\n":
                return EndFiltered[i:] +'\n'
		return ''

def displayLocalMessage(chatBox, messageText):
	#if there is no text, do nothing
    if messageText != '':
        chatBox.config(state=NORMAL)
        if chatBox.index('end') != None:
			#adds line of text to the end
            LineNumber = float(chatBox.index('end'))-1.0
			#adds text to chatBox
            chatBox.insert(END, "YOU: " + messageText)
			#tkinter functions to customize aesthetics
            chatBox.tag_add("[YOU]", LineNumber, LineNumber + 0.4)
            chatBox.tag_config("[YOU]", foreground="#AA3939", font=("Courier", 12, "bold"), justify="left")
            chatBox.config(state=DISABLED)
	chatBox.yview(END)

def displayRemoteMessage(chatBox, messageText):
    if messageText != '':
        chatBox.config(state=NORMAL)
        if chatBox.index('end') != None:
            try:
                LineNumber = float(chatBox.index('end'))-1.0
            except:
                pass
            chatBox.insert(END, "OTHER: " + messageText)
            chatBox.tag_add("OTHER", LineNumber, LineNumber+0.6)
            chatBox.tag_config("OTHER", foreground="#255E69", font=("Courier", 12, "bold"), , justify="right")
            chatBox.config(state=DISABLED)
    chatBox.yview(END)

def onEnterButtonPressed(event):
    textBox.config(state=NORMAL)
    onClick()

def removeKeyboardFocus(event):
	textBox.config(state=DISABLED)

def onClick():
    messageText = messageFilter(textBox.get("0.0", END)) #filter

    if "/shrug" in messageText :
        messageText =  "¯\_(ツ)_/¯"
        s.send(messageText)

    elif "/creep" in messageText :
        messageText = "( ͡° ͜ʖ ͡°)"
        s.send(messageText) #Just send the message
    elif "/smile" in messageText :
        messageText = "•ᴗ•"
        s.send(messageText) #Just send the message
    else:
    	print "ok"
        #s.send(messageText) #send over socket

    displayLocalMessage(chatBox, messageText)
    chatBox.yview(END)
    textBox.delete("0.0",END)





#Base Window
base = Tk()
base.title("Pychat Client")
base.geometry("400x450")
base.resizable(width=FALSE, height=FALSE)
base.configure(bg="#716664")

#Chat
chatBox = Text(base, bd=0, bg="#689099", height="8", width="20", font="Helvetica",)
chatBox.config(state=DISABLED)
sb = Scrollbar(base, command=chatBox.yview, bg = "#34495e")
chatBox['yscrollcommand'] = sb.set

#Send Button
sendButton = Button(base, font="Helvetica", text="SEND", width="50", height=5,
                    bd=0, bg="#BDE096", activebackground="#BDE096", justify="center",
                    command=onClick)

#Text Input
textBox = Text(base, bd=0, bg="#F8B486",width="29", height="5", font="Helvetica")
textBox.bind("<Return>", removeKeyboardFocus)
textBox.bind("<KeyRelease-Return>", onEnterButtonPressed)

#Put everything on the window
sb.place(x=370,y=5, height=350)
chatBox.place(x=15,y=5, height=350, width=355)
sendButton.place(x=255, y=360, height=80, width=130)
textBox.place(x=15, y=360, height=80, width=250)

base.mainloop()