#include "server.h"

#include <unistd.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

void 			send_msg(void *msg, t_serv_sock *s, int fd)
{
	char 		str[BUF_SIZE];
	char 		*tokken;

	sprintf(str, "[%s]", get_name(fd, s));
	if (!my_strstr(msg, str))
		send_message_self((char *)msg, fd);
	else
	{
		memset(str, '\0', BUF_SIZE);
		tokken = strtok((char *)msg, ":\n");
		tokken = strtok(NULL, ":\n");
		sprintf(str, "%s\n", tokken);
		sprintf(str, "[YOU]: %s\n", tokken);
		send_message_self(str, fd);
	}
}

void 			display_historic(t_serv_sock *s, char *group, int fd)
{
	int 		i;

	i = is_group_exist(s, group);
	display_list(s->channels[i]->historic, s, fd, send_msg);
}

void 			save_msg_to_historic(t_serv_sock *s
	, char *group_name
	, char *msg)
{
	int 		i;

	i = is_group_exist(s, group_name);
	append(s->channels[i]->historic, my_strdup(msg));
}