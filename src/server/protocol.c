#include "server.h"

#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/select.h>
#include <stdio.h>

//très sale faut changer ça!!!
void		print_menu(int fd)
{
	send_message_self("\033[H\033[2J\033[31m\n\t\tMENU\t\\H - Help\033[0m\n", fd);
	send_message_self("\033[35m\t\\N <group>	- Add new Group\033[0m\n", fd);
	send_message_self("\033[32m\t\\P <username>	- Private chat with user\033[0m\n", fd);
	send_message_self("\033[33m\t\\U 			- List online Users\033[0m\n", fd);
	send_message_self("\033[34m\t\\G <group>	- Change Group\033[0m\n", fd);
	send_message_self("\033[36m\t\\X 			- Exit Group\033[0m\n", fd);
	send_message_self("\033[35m\t\\L 			- List Group\033[0m\n", fd);
	send_message_self("\033[37m\t\\A <username>	- Chat with everyone !\033[0m\n", fd);
	send_message_self("\033[35m\t\\OFF 			- Set Offline\033[0m\n", fd);
	send_message_self("\033[33m\t\\C <username>	- Change your name\033[0m\n", fd);
	send_message_self("\033[31m\t\\Q 			- Quit\n\n\033[0m\n", fd);
}

void 		run_simple_cmd(t_serv_sock *server, char *token
	, int fd, fd_set *readfds)
{
	int 	i;

	i = get_id_client(server, fd);
	if (my_strstr(token, OFF_LINE))
	{
		server->clients[i]->online = FALSE;
		server->channels[server->clients[i]->chan_id]->user_con--;
		chat_with_all(server, "Went Offlne\n", fd);
		close(fd);
		FD_CLR(fd, readfds);
	}
	else if (my_strcmp(token, QUIT) == 0)
		queue_delete(fd, server);
	else if (my_strcmp(token, LIST_CHAN) == 0)
		display_groups_name(server, fd);
	else if (my_strcmp(token, HELP) == 0)
		print_menu(fd);
	else if (my_strcmp(token, LEAVE_G) == 0)
		leave_group(server, fd);
	else if (my_strcmp(token, LIST_USER) == 0)
		send_active_clients(server->clients, fd, server->client_no);
}

void 		run_cmd(t_serv_sock *server, char *cmd, char *arg
	, char *msg, int fd)
{
	if (my_strcmp(cmd, CHAT) == 0)
	{
		if (arg && msg)
			send_priv_msg(server, msg, fd, arg);
	}
	else if (my_strcmp(cmd, CHANGE_G) == 0)
	{
		if (arg)
			change_group(server, arg, fd);
	}
	else if (my_strcmp(cmd, ADD_G) == 0)
	{
		if (arg)
			add_group(server, arg, fd);
	}
	else if (my_strcmp(cmd, CHANGE_NAME) == 0)
	{
		if (arg)
			change_name(server, arg, fd);
	}
}

void 		parse_proto(t_serv_sock *server, char *cmd
	, int fd, fd_set *readfds)
{
	char	*token[BUF_SIZE];
	char 	*tmp;

	//check if the user send a specific cmd
	tmp = strtok(my_strdup(cmd), PROTO_SEP);
	if (!tmp)
		return ;
	if (tmp && tmp[0] == '\\')
	{
		token[0] = strsep(&cmd, PROTO_SEP);
		run_simple_cmd(server, token[0], fd, readfds);
		if (my_strcmp(token[0], ALL) == 0 && cmd)
			chat_with_all(server, cmd, fd);
		token[1] = strsep(&cmd, PROTO_SEP);
		run_cmd(server, token[0], token[1], cmd, fd);
	}
	else
		send_message_to_group(server, get_chan(fd, server), cmd, fd);
}