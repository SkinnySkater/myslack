#include "server.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>

void                        error(char *msg)
{
    my_perror(msg);
    return ;
}

t_serv_sock                 *_error(t_serv_sock *server, char *msg)
{
    my_perror(msg);
    server->error = TRUE;
    return (server);
}

void                        _init_channels(t_serv_sock *server_sock)
{
    t_channel               **channels;
    t_channel               *channel;
    t_list                  *hist;

    channels = malloc(sizeof(*channel) * MAX_CHANNELS);
    channel = malloc(sizeof(*channel));
    hist = malloc(sizeof(hist));
    my_strcpy(channel->name, DEFAULT_CHAN);
    channel->user_con = 0;
    hist = create("", NULL);
    channel->historic = hist;
    channels[0] = channel;
    channel = malloc(sizeof(*channel));
    hist = malloc(sizeof(hist));
    my_strcpy(channel->name, "skate");
    channel->user_con = 0;
    channels[1] = channel;
    hist = create("", NULL);
    channel->historic = hist;
    server_sock->channels = channels;
    server_sock->channel_no = 1;
}

void                        _init_clients(t_serv_sock *server_sock)
{
    int                     i;
    client_t                **clients;
    client_t                *client;

    clients = malloc(sizeof(client_t) * MAX_USERS);
    for (i = 0; i < MAX_USERS; i++)
    {
        client = (client_t *)malloc(sizeof(client_t *));
        clients[i] = client;
    }
    server_sock->clients = clients;
}

t_serv_sock                 *init_server()
{
    t_serv_sock             *server;

    server = malloc(sizeof(server));
    _init_channels(server);
    _init_clients(server);
    server->error = FALSE;
    server->client_no = 0;
    return (server);
}