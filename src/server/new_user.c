#include "server.h"

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>
#include <unistd.h>



void             loop_fun(t_serv_sock *server, char *buf, int newfd, int *loop)
{
    char         *s;

    if (is_user_exist(buf, server->clients) || my_isspace(buf))
    {
        s = "\033[31m[error]   Username unavailable!\033[0m";
        send(newfd, s, my_strlen(s), 0);
        *loop = 1;
    }
    if (server->client_no >= MAX_USERS - 3)
    {
        s = "\033[31m[error]   The server is already full!\n\033[0m";
        send(newfd, s, my_strlen(s), 0);
        *loop = 1;
    }
}

void                    welcome_user(t_serv_sock *server, int newfd)
{
    server->client_no = newfd;
    //send new connection greeting message 
    if (send(newfd, WELCOME_MSG, my_strlen(WELCOME_MSG), 0)
     != my_strlen(WELCOME_MSG))
        perror("send");
    my_putstr("Welcome message sent successfully\n");
    //add new socket to array of sockets
    my_putstr_color("green", "Adding to list of sockets as ");
    my_put_nbr(newfd);
    my_putstr("\n");
}

void               notify_user(t_serv_sock *server, int newfd, int isback)
{
    char           str[BUF_SIZE];

    memset(str, '\0', BUF_SIZE);
    if (isback)
        server->channels[server->clients[get_id_client(server, newfd)]->chan_id]->user_con++;
    server->clients[get_id_client(server, newfd)]->online = TRUE;
    sprintf(str, "\nYou are now online as %s!\n[Group]:%s\n"
        , get_name(newfd, server), get_chan(newfd, server));
    write(newfd, str, sizeof(str));
    display_historic(server, DEFAULT_CHAN, newfd);
    sprintf(str, "\n\033[34m[%s is connected!]\033[0m\n"
        , get_name(newfd, server));
    send_message(server->clients, str, newfd);
}

void               handle_client(t_serv_sock *server, int newfd, int max_sd)
{
    int            *loop;
    int            l;
    int            isback;
    char           buf[BUF_SIZE];

    l = 1;
    isback = newfd <= max_sd;
    if (newfd > max_sd)
    {
        loop = &l;
        while (*loop)
        {
            *loop = 0;
            memset(buf, '\0', BUF_SIZE);
            write(newfd, REGISTRATION, sizeof(REGISTRATION));
            recv(newfd, buf, sizeof(buf), 0);
            loop_fun(server, buf, newfd, loop);
        }
        queue_add(newfd, buf, server);
        welcome_user(server, newfd);
    }
    notify_user(server, newfd, isback);
}

void                    new_user(t_serv_sock *server
    , int *master_socket
    , int max_sd)
{
    struct sockaddr_in  address;
    int                 newfd;
    int                 addrlen;
    char                *buf = malloc(100);
    
    addrlen = sizeof(address);
    if (!((newfd = accept(*master_socket
        , (struct sockaddr *)&address, (socklen_t*)&addrlen)) == -1))
    {
        sprintf(buf, "New connection , socket fd is %d , ip is : %s , port : %d\n"
            , newfd, inet_ntoa(address.sin_addr), ntohs(address.sin_port));
        my_putstr(buf);
        handle_client(server, newfd, max_sd);
    }
    free(buf);
}