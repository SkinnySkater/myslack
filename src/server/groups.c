#include "server.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>
	

void			welcome_group(char *group, int fd)
{
	char 		str[BUF_SIZE];
	char 		*tmp = "===============================================";
	char  		*help = "- Exit(\\x)";
	char 		*init = "\033[H\033[2J\033[33m";

	sprintf(str, "%s\n\t%s\n\t You are in |%s| Group! %s \n\t%s\n\033[0m"
		,init, tmp, group, help, tmp);
	send(fd, str, my_strlen(str), 0);
}

int				group_connection(t_serv_sock *s, char *group)
{
	int 		i;

	for (i = 0; i <= s->channel_no; i++)
	{
		if (s->channels[i] && my_strcmp(s->channels[i]->name, group) == 0)
			return (i);
	}
	return (FAIL);
}	

void 			apply_changes(t_serv_sock *s, char *group, int fd, int i)
{
	char 		str[BUF_SIZE];

	//change the increment number of people
	sprintf(str, "[%s] has left the channel !\n", s->clients[i]->name);
	send_message_channel(s, str, s->channels[s->clients[i]->chan_id]->name
		, fd);
	s->channels[group_connection(s
		, s->channels[s->clients[i]->chan_id]->name)]->user_con -= 1;
	s->channels[group_connection(s, group)]->user_con += 1;
	sprintf(str, "\033[33m[%s] has joined the Group ;)\n\033[0m"
		, get_name(fd, s));
	send_message_channel(s, str, group, fd);
	s->clients[i]->chan_id = group_connection(s, group);
	welcome_group(group, fd);
	display_historic(s, group, fd);
}

void 			change_group(t_serv_sock *s, char *group, int fd)
{
	int 		i;
	char 		str[BUF_SIZE];
	char 		*tmp;

	if (is_group_exist(s, group) >= 0)
	{
		for (i = 0; i < MAX_USERS; i++)
		{
			if (s->clients[i] && s->clients[i]->client_socket == fd
			 && !my_strcmp(s->channels[s->clients[i]->chan_id]->name, group))
			{
				sprintf(str
					, "\033[33mYou are already in the Group [%s] ;)\n\033[0m"
					, group);
				write(fd, str, my_strlen(str));
				return ;
			}
			else if (s->clients[i] && s->clients[i]->client_socket == fd)
				return apply_changes(s, group, fd, i);
		}
	}
	else
	{
		tmp = "\n\t\033[33mYou may want to create this group first.\033[0m\n";
		sprintf(str, "\033[31m\t Sorry group [%s] does not seem to exist...%s", group, tmp);
		write(fd, str, my_strlen(str));
	}
}

void 			send_message_to_group(t_serv_sock *server
	, char *group, char *msg, int fd)
{
	char 		str[BUF_SIZE];

	sprintf(str, "\033[32m\t\t\t\t[%s]: %s\n\033[0m", get_name(fd, server), msg);
	save_msg_to_historic(server, group, str);
	send_message_channel(server, str, group, fd);
}