#include <stdlib.h>
#include "server.h"
#include "../util/util.h"

#include <stdio.h>
#include <string.h>

int				is_user_exist(char *username, client_t **clients)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (clients[i] && my_strcmp(clients[i]->name, username) == 0)
			return (1);
	}
	return (0);
}

int 			is_user_in_group(int fd, char* channel, t_serv_sock *s)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i] && s->clients[i]->client_socket == fd)
		{
			if (my_strcmp(s->channels[s->clients[i]->chan_id]->name
				, channel) == 0)
				return (1);
		}
	}
	return (0);
}

/* Add client to queue */
void            queue_add(int cl_fd, char *name, t_serv_sock *server)
{
    int       i;

    for (i = 0; i < MAX_USERS; i++)
    {
        if (server->clients[i] && server->clients[i]->client_socket == 0)
        {
            server->clients[i]->client_socket = cl_fd;
            my_strcpy(server->clients[i]->name, name);
            server->clients[i]->chan_id = 0;
            server->channels[0]->user_con += 1;
            return ;
        }
    }
}

void 			queue_delete(int fd, t_serv_sock *server)
{
	int 	i;
	char		str[BUF_SIZE];

	sprintf(str, "\033[31m[%s] Leave the server.\n\033[0m", get_name(fd, server));
	i = get_id_client(server, fd);
	server->channels[server->clients[i]->chan_id]->user_con--;
	server->clients[i]->client_socket = 0;
	server->client_no--;
	memset(server->clients[i]->name, '\0', 64);
	send_message(server->clients, str, fd);
	send_message_self(KILL, fd);
}