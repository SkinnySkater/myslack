#include "server.h"

#include <stdio.h>
#include <unistd.h>

/* Add client's channel's name */
char 			*get_chan(int fd, t_serv_sock *s)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i] && s->clients[i]->client_socket == fd)
			return (s->channels[s->clients[i]->chan_id]->name);
	}
	return (NULL);
}

char			*get_name(int fd, t_serv_sock *server)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (server->clients[i] && server->clients[i]->client_socket)
		{
			if (server->clients[i]->client_socket == fd)
				return (server->clients[i]->name);
		}
	}
	return (NULL);
}

client_t 		*get_client_by_fd(int fd, t_serv_sock *server)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (server->clients[i] && server->clients[i]->client_socket)
		{
			if (server->clients[i]->client_socket == fd)
				return (server->clients[i]);
		}
	}
	return (NULL);
}

int				is_group_exist(t_serv_sock *s, char *group)
{
	int 		i;

	for (i = 0; i <= s->channel_no; i++)
	{
		if (my_strcmp(s->channels[i]->name, group) == 0)
			return (i);
	}
	return (FAIL);
}

int				get_id_client(t_serv_sock *s, int fd)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i]->client_socket == fd)
			return (i);
	}
	return (FAIL);
}