#include "server.h"

#include <sys/time.h>
#include <stdio.h>

void        my_error(char *s, int *i)
{
    perror(s);
    *i = -1;
}

void        init_clients(t_serv_sock *server)
{
    int     i;

    for (i = 0; i < MAX_USERS; i++)
    {
        server->clients[i]->online = TRUE;
        server->clients[i]->chan_id = 0;
        my_strcpy(server->clients[i]->name, "default");
    }
}

void        reinit_loop(t_serv_sock *server, fd_set *readfds
    , int *master_socket, int *max_sd)
{
    int     i;
    int     sd;

    //clear the socket set
    FD_ZERO(readfds);
    //add master socket to set 
    FD_SET(*master_socket, readfds);  
    *max_sd = *master_socket;
    //add child sockets to set
    for (i = 0; i < MAX_USERS; i++)
    {
        sd = server->clients[i]->client_socket;
        //if valid socket descriptor then add to read list
        if (sd > 0 && server->clients[i]->online)
            FD_SET(sd, readfds);
        //highest file descriptor number, need it for the select function
        if (sd > *max_sd)
            *max_sd = sd;
    }
}

int             get_id_client_by_name(t_serv_sock *s, char* name)
{
    int         i;

    for (i = 0; i < MAX_USERS; i++)
    {
        if (!my_strcmp(s->clients[i]->name, name))
            return (i);
    }
    return (FAIL);
}
