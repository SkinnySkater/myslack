#include "server.h"

#include <stdio.h>
#include <unistd.h>
#include <string.h>

void			display_groups_name(t_serv_sock *s, int fd)
{
	int 		i;
	char 		str[BUF_SIZE];

	for (i = 0; i <= s->channel_no; i++)
	{
		sprintf(str, "\033[33mGroup[%d]: %s [%d] users in the group\033[0m\n", i
			, s->channels[i]->name, s->channels[i]->user_con);
		write(fd, str, my_strlen(str) * sizeof(char));
	}
}

void 			add_group(t_serv_sock *server, char *group, int fd)
{
	char 		str[BUF_SIZE];
	t_channel   *channel;

	if (server->channel_no == MAX_CHANNELS)
	{
		sprintf(str, "\033[31m Number of channel exceeded ! \n\033[0m");
		write(fd, str, my_strlen(str) * sizeof(char));
	}
	else if (is_group_exist(server, group) >= 0)
	{
		sprintf(str, "\033[31m This group already exists! \n\033[0m");
		write(fd, str, my_strlen(str) * sizeof(char));
	}
	else
	{
		server->channel_no += 1;
		channel = malloc(sizeof(*channel));
		my_strcpy(channel->name, group);
		channel->user_con = 0;
		channel->historic = create("", NULL);
		server->channels[server->channel_no] = channel;
		sprintf(str, "\033[33m Channel [%s] has	been added ! \n\033[0m", group);
		send_message_all(server->clients, str);
	}
}

void 			leave_group(t_serv_sock *s, int fd)
{
	int 		i;
	char 		str[BUF_SIZE];

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i] && s->clients[i]->client_socket == fd && s->clients[i]->chan_id)
		{
			s->channels[group_connection(s, s->channels[get_client_by_fd(fd, s)->chan_id]->name)]->user_con--;
			s->channels[group_connection(s, DEFAULT_CHAN)]->user_con++;
			sprintf(str, "[%s] has left the channel !\n", s->clients[i]->name);
			send_message_channel(s, str, s->channels[s->clients[i]->chan_id]->name, fd);
			s->clients[i]->chan_id = 0;
			welcome_group(DEFAULT_CHAN, fd);
			display_historic(s, DEFAULT_CHAN, fd);//---------------------------
			my_memset(str , 0, BUF_SIZE * sizeof(char));
			sprintf(str, "\033[33m%s has joined the Group ;)\n\033[0m", s->clients[i]->name);
			send_message_channel(s, str, DEFAULT_CHAN, fd);
			return ;
		}
	}
	send_message_self("\033[34m\t\\You are already in the default group ;]\033[0m\n", fd);
}

char 			*get_group_name_by_fd(t_serv_sock *s, int fd)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i] && s->clients[i]->client_socket == fd)
			return (s->channels[s->clients[i]->chan_id]->name);
	}
	return (NULL);
}
