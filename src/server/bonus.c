#include "server.h"
#include <stdio.h>

void 		change_name(t_serv_sock *s, char *new, int fd)
{
	char 		str[BUF_SIZE];
	int 		i;

	if (my_isspace(new))
	{
		sprintf(str, "\033[33m Your name Is unavailable! \n\033[0m");
		send_message_self(str, fd);
	}
	else if (is_user_exist(new, s->clients))
	{
		sprintf(str, "\033[33m This Username already exists! \n\033[0m");
		send_message_self(str, fd);
	}
	else
	{
		sprintf(str
			, "\033[33m[server] '%s' change his name to '%s' ! \n\033[0m"
			, get_name(fd, s), new);
		i = is_user_exist(get_name(fd, s), s->clients);
		my_memset(s->clients[i]->name, 0, 64);
		my_strcpy(s->clients[i]->name, new);
		send_message_all(s->clients, str);
	}
}
