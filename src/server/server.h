#ifndef		_SERVER_H_
# define	_SREVER_H_

#define TRUE   			1
#define FALSE  			0
#define FAIL			-1

#define USER_INFO 		1024
/* CONFIGURATION */
#define LISTEN_PORT		8888
#define PENDING_CON 	5
#define BUF_SIZE		1024

#define HISTORIC_SIZE 	2048
#define MAX_USERS   	100
#define MAX_CHANNELS	50
#define GROUP_NAME 		64

#define PROTO_SEP 		" \n"

#define SERVER_NAME		"MySlack"
#define DEFAULT_CHAN	"general"
#define	WELCOME_MSG 	"#Thanatos Slack ChatRoom#\n"
#define CLEAR 			"\033[H\033[2J"

/* CLIENT MACRO */
#define SERVER_IP   	"127.0.0.1"
#define SERVER_PORT 	8888

#define QUIT         	"\\q"
#define LIST_CHAN    	"\\l"
#define LIST_USER    	"\\u"
#define CHAT         	"\\p"
#define ALL          	"\\a"
#define HELP         	"\\h"
#define LEAVE_G      	"\\x"
#define CHANGE_G     	"\\g"
#define ADD_G        	"\\n"
#define QUIT         	"\\q"
#define LIST_CHAN    	"\\l"
#define OFF_LINE    	"\\off"
#define CHANGE_NAME 	"\\c"

#define REGISTRATION	"r@"
#define BACK_ONLIONE 	"@BO"
#define KILL 			"888"

#define BELL 			"\a"

#include "../util/util.h"


#include <sys/types.h> 
#include <sys/socket.h> 
#include <netinet/in.h> 
#include <sys/time.h>

/* historic list struct */
typedef struct     s_list
{
  void             *data;
  struct s_list    *next;
} t_list;

/* Channel structure */
typedef struct {
	char 		name[GROUP_NAME];
	t_list 		*historic;
	int 		user_con;
} t_channel;

/* Client structure */
typedef struct {
	int 		chan_id;				/* Current Channel name -> default to 'default' */
	int 		client_socket;			/* Connection file descriptor */
	int 		online;					/* Online  */
	char 		name[64];				/* Client name */
} client_t;

/* Server Init Socket structure */
typedef struct {
	int 		client_no;
	client_t 	**clients;
	int 		error;
	int 		channel_no;
	t_channel	**channels;
} t_serv_sock;

void 			prompt();

void       	 	error(char *msg);

t_serv_sock		*init_server();

void            add_user(t_serv_sock *server, int *no_clients, fd_set *active_fd_set, int *sock_recv);

void			handle_protocol(t_serv_sock *server_sock, fd_set *readfds, int *addrlen);

void			welcome_group(char *group, int fd);

void            new_user(t_serv_sock *server, int *master_socket, int max_sd);

int				get_id_client(t_serv_sock *s, int fd);

int             get_id_client_by_name(t_serv_sock *s, char* name);

/* QUEUE METHODS */

void 			queue_add(int cl_fd, char *name, t_serv_sock *server);

void 			queue_delete(int fd, t_serv_sock *server);

int				is_user_exist(char *username, client_t **clients);

char			*get_name(int fd, t_serv_sock *server);

client_t 		*get_client_by_fd(int fd, t_serv_sock *server);

char 			*get_chan(int fd, t_serv_sock *server);

/* MESSAGE METHODS */
void 			send_message(client_t **clients, char *s, int id);

void 			send_message_all(client_t **clients, char *s);

void 			send_message_self(char *s, int connfd);

void 			send_active_clients(client_t **clients, int client_socket, int users);

void 			send_priv_msg(t_serv_sock *server, char *msg, int sender_fd, char *name);

void 			chat_with_all(t_serv_sock *server, char *msg, int fd);

void 			send_message_channel(t_serv_sock *s, char *msg, char *chan_name, int fd);

/* GROUPS METHODS */
void 			change_group(t_serv_sock *server, char *group, int fd);

void			display_groups_name(t_serv_sock *server, int fd);

int				is_group_exist(t_serv_sock *server, char *group);

void 			add_group(t_serv_sock *server, char *group, int fd);

void 			leave_group(t_serv_sock *server, int fd);

void 			send_message_to_group(t_serv_sock *server, char *group, char *msg, int fd);

char 			*get_group_name_by_fd(t_serv_sock *server, int fd);

int				group_connection(t_serv_sock *server, char *group);

/* HISTORIC */
void 			save_msg_to_historic(t_serv_sock *s, char *group_name, char *msg);

void 			display_historic(t_serv_sock *server, char *group, int fd);

/* PROTOCOL PARSER */
void 			parse_proto(t_serv_sock *server, char *cmd, int fd, fd_set *readfds);

/* SERVER USAGE */
void 			my_error(char *s, int *i);

void        	init_clients(t_serv_sock *server);

void        	reinit_loop(t_serv_sock *server, fd_set *readfds, int *master_socket, int *max_sd);

/* historic list functions */

t_list		*create(void *data, t_list *next);

t_list		*append(t_list *head, void *data);

void  		my_apply_on_list(t_list *begin, int (*f)());

void      	display_list(t_list *node, t_serv_sock *s, int fd
			, void (*fptr)(void *, t_serv_sock *s, int));

/* Bonus commands */
void 		change_name(t_serv_sock *s, char *new, int fd);

#endif		/* !_SERVER_H_ */