#include <unistd.h>
#include <stdio.h>

#include "server.h"
#include "../util/util.h"

/* Send message to all clients but the sender and listener */
void 		send_message(client_t **clients, char *s, int id)
{
	int 	i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (clients[i] && clients[i]->client_socket)
		{
			if (clients[i]->client_socket != id)
				write(clients[i]->client_socket, s, my_strlen(s));
		}
	}
}

/* Send message to all clients */
void 			send_message_all(client_t **clients, char *s)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (clients[i] && clients[i]->client_socket)
			write(clients[i]->client_socket, s, my_strlen(s));
	}
}

void 			chat_with_all(t_serv_sock *server, char *msg, int fd)
{
	int 		i;
	char 		s[BUF_SIZE];

	sprintf(s, "%s\033[31mGroup(%s)[%s]: %s\n\033[0m", BELL
		, get_group_name_by_fd(server, fd)
		, get_name(fd, server)
		, msg);
	for (i = 0; i < MAX_USERS; i++)
	{
		if (server->clients[i] && server->clients[i]->client_socket
		 && server->clients[i]->client_socket != fd)
			write(server->clients[i]->client_socket, s, my_strlen(s));
	}
}

/* Send message to sender */
void 			send_message_self(char *s, int connfd)
{
	write(connfd, s, my_strlen(s));
}

/* Send message to client */
void 			send_priv_msg(t_serv_sock *server, char *msg
	, int sender_fd, char *name)
{
	int 		i;
	char 		s[BUF_SIZE];

	i = get_id_client_by_name(server, name);
	if (i >= 0)
	{
		if (server->clients[i]->online == FALSE)
		{
			sprintf(s, "%s\033[36m[Private][%s]: is actually offline\n\033[0m"
				, BELL, get_name(sender_fd, server));
			send_message_self(s, sender_fd);
		}
		else
		{
			sprintf(s, "%s\033[36m[Private][%s]: %s\n\033[0m", BELL
				, get_name(sender_fd, server), msg);
			send_message_self(s, server->clients[i]->client_socket);
		}
	}
	else
	{
		sprintf(s, "%s%s\033[31mThe User %s doesn't exit! :(\n\033[0m"
			, BELL, BELL, name);
		send_message_self(s, sender_fd);
	}
}

/* Send list of active clients */
void 			send_active_clients(client_t **clients, int client_socket, int users)
{
	int 		i;
	char 		s[BUF_SIZE];

	sprintf(s, "\033[31m%s[%d] Users connected.\n\033[0m", BELL, users - 3);
	send_message_self(s, client_socket);
	for (i = 0; i < MAX_USERS; i++)
	{
		//if sender , write 'YOU' instead of USer
		if (clients[i] && clients[i]->client_socket == client_socket)
		{
			sprintf(s
				, "\033[33m[YOU]\t %s - %sLine\n\033[0m"
				, clients[i]->name, clients[i]->online ? "ON" : "Off");
			send_message_self(s, client_socket);
		}
		else if (clients[i] && clients[i]->client_socket)
		{
			sprintf(s, "\033[36m[USER]\t %s - %sLine\n\033[0m"
				, clients[i]->name
				, clients[i]->online ? "ON" : "Off");
			send_message_self(s, client_socket);
		}
	}
}

char 			*get_group_name_by_fd2(t_serv_sock *s, int fd)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i] && s->clients[i]->client_socket == fd)
			return (s->channels[s->clients[i]->chan_id]->name);
	}
	return NULL;
}


/* Send message to everyone from the same channel */
void 			send_message_channel(t_serv_sock *s
	, char *msg
	, char *chan_name, int fd)
{
	int 		i;

	for (i = 0; i < MAX_USERS; i++)
	{
		if (s->clients[i])
		{
			if (s->clients[i]->client_socket != 0
			 && s->clients[i]->client_socket != fd)
			{
				if (!my_strcmp(s->channels[s->clients[i]->chan_id]->name
					, chan_name))
					write(s->clients[i]->client_socket, msg, my_strlen(msg));
			}
		}
	}
}