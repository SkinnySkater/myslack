#ifndef		_CLIENT_USAGE_H_
# define	_CLIENT_USAGE_H_

#include "../server/server.h"

#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <stdio.h>


void                    send_msg_serv(int sock, char *buf);

int                     check_srv_off(int i, int byte_recvd, fd_set *read_fds);

int                     login(char **buf, int sock);

int                     perr(char *s);

/* CHECK INPUT */
int                		print_usage(char *prgm);

int 					check_port(char *port_str);

#endif		/* !_CLIENT_USAGE_H_ */