#include "client.h"


void                    send_msg_serv(int sock, char *buf)
{
    bzero(buf, BUF_SIZE);
    memset(buf, '\0', BUF_SIZE);
    buf = readLine();
    write(sock, buf, my_strlen(buf));
}

int                     check_srv_off(int i, int byte_recvd, fd_set *read_fds)
{
    if (byte_recvd <= 0)
    {
        if (byte_recvd == 0)
            my_putstr_color("red", "Server offline\n");
        else
            perror("recv");
        FD_CLR(i, read_fds);
        return (1);
    }
    return (0);
}

int                     login(char **buf, int sock)
{
    if (!my_strcmp(*buf, REGISTRATION) || my_strstr(*buf, REGISTRATION))
    {
        my_putstr_color("cyan", "\n[server]  Enter a username? : ");
        memset(*buf, '\0', BUF_SIZE);
        *buf = readLine();
        write(sock, *buf, my_strlen(*buf));
        return (1);
    }
    return (0);
}

int                     perr(char *s)
{
    perror(s);
    return (1);
}
