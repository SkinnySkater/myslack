#include "client.h"

#include <stdlib.h>

int                print_usage(char *prgm)
{
    char            *msg = malloc(500);

    sprintf(msg, "usage: %s <PORT_SERVER> <IP_SERVER>\n", prgm);
    my_putstr_color("red", msg);
    sprintf(msg, "\nPORT_SERVER:    [0-65535]\n");
    my_putstr_color("red", msg);
    sprintf(msg, "\nIP_SERVER:      [X.X.X.X]\n");
    my_putstr_color("red", msg);
    free(msg);
    return (1);
}

/* error return -1, return port number otherwise */
int 				check_port(char *port_str)
{
	int 			portno;
	char 			*msg = malloc(500);

	if (!is_nbr(port_str))
    {
        sprintf(msg, "<PORT_SERVER> has to be an Integer.\n");
        my_putstr_color("red", msg);
        free(msg);
        return (-1);
    }
    portno = my_getnbr(port_str);
    if (portno < 0 || portno > 65535)
    {
        sprintf(msg, "<PORT_SERVER> has to be between [0-65535].\n");
        my_putstr_color("red", msg);
        free(msg);
        return (-1);
    }
    free(msg);
    return (portno);
}