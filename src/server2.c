#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/time.h>
#include <errno.h>

#include "server/server.h"

struct sockaddr_in          setup_address(int *master_socket, int *i)
{
    struct sockaddr_in      address;
    int opt;

    opt = TRUE;
    *i = 0;
    //create a master socket 
    if ((*master_socket = socket(AF_INET , SOCK_STREAM , 0)) == 0)
        my_error("socket failed", i);
    //Avoid the sticky bind error
    if (setsockopt(*master_socket, SOL_SOCKET, SO_REUSEADDR
        , (char *)&opt, sizeof(opt)) < 0)
        my_error("socket options", i);
    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons(LISTEN_PORT);
    if (bind(*master_socket, (struct sockaddr *)&address
        , sizeof(address)) < 0)
        my_error("bind failed", i);
    my_putstr_color("green", "\033[H\033[2JListening...\n");
    if (listen(*master_socket, PENDING_CON) < 0)
        my_error("listen failed", i);
    return (address);
}

void        print_log_send(int sd, struct sockaddr_in address
    , t_serv_sock *s, char *buffer)
{
    char    *buf = malloc(500);

    sprintf(buf, "User [%s] SEND %s, ip %s, port %d\n"
        , get_name(sd, s)
        , buffer
        , inet_ntoa(address.sin_addr)
        , ntohs(address.sin_port));
    my_putstr_color("green", buf);
    free(buf);
}

void        print_log_disconect(int sd, struct sockaddr_in address
    , t_serv_sock *s)
{
    char    buf[500];

    sprintf(buf, "User [%s] Diconnected: ip %s, port %d\n"
        , get_name(sd, s)
        , inet_ntoa(address.sin_addr)
        , ntohs(address.sin_port));
    my_putstr_color("red", buf);
}

void        handle_fd_client(t_serv_sock *server, fd_set *readfds
    , struct sockaddr_in address, int i)
{
    int     sd;
    int     addrlen;
    int     valread;
    char    buffer[BUF_SIZE + 1];

    addrlen = sizeof(address);
    if (server->clients[i])
    {
        sd = server->clients[i]->client_socket;
        if (FD_ISSET(sd , readfds))
        {
            getpeername(sd, (struct sockaddr*)&address, (socklen_t*)&addrlen);
            if ((valread = read(sd, buffer, BUF_SIZE)) == 0)
            {
                print_log_disconect(sd, address, server);
                queue_delete(sd, server);
            }
            else
            {
                buffer[valread] = '\0';
                print_log_send(sd, address, server, buffer);
                parse_proto(server, buffer, sd, readfds);
            }
        }
    }
}

int             main(void)  
{
    int                 master_socket;
    int                 activity;
    int                 i;
    int                 max_sd;
    struct sockaddr_in  address;
    t_serv_sock         *server;
    fd_set              readfds;

    server = init_server();
    init_clients(server);
    address = setup_address(&master_socket, &i);
    if (i < 0)
        return (1);
    my_putstr_color("green", "Waiting for connections ...\n");
    while (TRUE)
    {
        reinit_loop(server, &readfds, &master_socket, &max_sd);
        activity = select(max_sd + 1, &readfds, NULL, NULL, NULL);
        if ((activity < 0) && (errno != EINTR))
            perror("select error");
        if (FD_ISSET(master_socket, &readfds))
            new_user(server, &master_socket, max_sd);
        for (i = 0; i < MAX_USERS; i++)
            handle_fd_client(server, &readfds, address, i);
    }
    return (0);
}