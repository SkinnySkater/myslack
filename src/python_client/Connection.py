#!/usr/bin/python3

import socket, select, sys
from tkinter import *
import tkMessageBox

class Connection:
   connection_fields = "Host", "Port"

   def __init__(self):
      self.ok     = False
      self.s      = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
      self.host   = "X.X.X.X"
      self.port   = -1
      self.exit   = True
      self.root   = Tk()
      self.root.title("Connection - MySlack V1.0")
      self.root.resizable(0, 0)
      self.root.protocol('WM_DELETE_WINDOW', exit)
      ents        = self.makeform()
      self.root.bind('<Return>', (lambda event, e=ents: self.fetch(e)))
      b2 = Button(self.root, text='Connect!', command=(lambda e=ents: self.fetch(e)))
      b2.pack(padx=5, pady=5)
      self.root.mainloop()

   def fetch(self, entries):
      for entry in entries:
         field = entry[0]
         text  = entry[1].get()
         if (not text):
            warning = "Please Complete the field: " + field
            tkMessageBox.showwarning("Error", warning)
            return
      #good parsing
      self.host = entries[0][1].get()
      self.port = entries[1][1].get()
      print('Host: "%s"\nPort: "%s"' % (self.host, self.port))
      try :
         self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
         self.s.settimeout(5)
         self.s.connect((self.host, int(self.port)))
         self.ok = True
         self.root.destroy()
      except :
         print 'Unable to connect'
         msg0 = "Unable to connect\n"
         msg1 = "Port follows the format: X.X.X.X\n"
         msg2 = "Port follows the format: DIGIT [0-65335]\n"
         msg3 = "Or Server might be unreachable"
         tkMessageBox.showwarning("Error", msg0 + msg1 + msg2)


   def makeform(self):
      entries = []
      for field in Connection.connection_fields:
         row = Frame(self.root)
         lab = Label(row, width=15, text=field, anchor='w')
         ent = Entry(row)
         row.pack(side=TOP, fill=X, padx=5, pady=5)
         lab.pack(side=LEFT)
         ent.pack(side=RIGHT, expand=YES, fill=X)
         ent.insert(10, "127.0.0.1")
         entries.append((field, ent))
      return entries

   def quit(self):
       self.root.quit()

   def exit(self):
      sys.exit