# -*- coding: utf-8 -*-
#!/usr/bin/python3

import socket, select, sys
from tkinter import *
import tkMessageBox

class Registration:
   username_fields = "Username",

   def __init__(self, connect, sock):
		self.connect = connect
		self.sock 	= sock
		self.ok     = False
		self.exit   = True
		self.username = ""
		self.root   = Tk()
		self.root.title("Registration - MySlack V1.0")
		self.root.resizable(0, 0)
		self.root.protocol('WM_DELETE_WINDOW', exit)
		ents        = self.makeform()
		self.root.bind('<Return>', (lambda event, e=ents: self.fetch(e)))
		b2 = Button(self.root, text='Connect!', command=(lambda e=ents: self.fetch(e)))
		b2.pack(padx=5, pady=5)
		self.root.mainloop()

   def fetch(self, entries):
		data = ""
		text =  entries[0][1].get()
		if (not text):
			warning = "The username can't be Empty."
			tkMessageBox.showwarning("Error", warning)
			return
		#good parsing
		self.username = entries[0][1].get()
		self.connect.send(self.username)
		data = self.sock.recv(1025)
		if not data :
			print '\nDisconnected from chat server'
			sys.exit()
		if (data.find("error") >= 0):
			tkMessageBox.showwarning("Error", data)
			return
		self.ok = True
		self.root.destroy()


   def makeform(self):
		entries = []
		for field in Registration.username_fields:
			row = Frame(self.root)
			lab = Label(row, width=15, text=field, anchor='w')
			ent = Entry(row)
			row.pack(side=TOP, fill=X, padx=5, pady=5)
			lab.pack(side=LEFT)
			ent.pack(side=RIGHT, expand=YES, fill=X)
			entries.append((field, ent))
		return entries

   def quit(self):
		self.root.quit()

   def exit(self):
		sys.exit