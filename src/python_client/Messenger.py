# -*- coding: utf-8 -*-
#!/usr/bin/python3

import socket, select, sys
from tkinter import *
import tkMessageBox
import thread
import string
import re

from   Registration import *

KILL            = "888"

REGISTRATION    = "r@"


class Messenger:

	def __init__(self, s):
		self.s = s
		self.root = Tk()
		self.root.title(" - MySlack V1.0")
		self.root.geometry("400x450")
		self.root.resizable(width=FALSE, height=FALSE)
		self.root.configure(bg="#679898")
		self.root.protocol('WM_DELETE_WINDOW', exit)
		#Chat
		self.chatBox = Text(self.root, bd=0, bg="#676798", height="8", width="20", font="Helvetica",)
		self.chatBox.config(state=DISABLED)
		sb = Scrollbar(self.root, command=self.chatBox.yview, bg = "#000000")
		self.chatBox['yscrollcommand'] = sb.set

		#Send Button
		self.sendButton = Button(self.root, font="Helvetica", text="SEND", width="50", height=5,
		                    bd=0, bg="#95dfdf", activebackground="#95badf", justify="center",
		                    command=self.onClick)

		#Text Input
		self.textBox = Text(self.root, bd=0, bg="#676798", fg="#000000", width="29", height="5", font="Helvetica")
		self.textBox.bind("<Return>", self.removeKeyboardFocus)
		self.textBox.bind("<KeyRelease-Return>", self.onEnterButtonPressed)

		#Put everything on the window
		sb.place(x=370,y=5, height=350)
		self.chatBox.place(x=15,y=5, height=350, width=355)
		self.sendButton.place(x=255, y=360, height=80, width=130)
		self.textBox.place(x=15, y=360, height=80, width=250)


		thread.start_new_thread(self.ReceiveData,())
		self.root.mainloop()


	def onEnterButtonPressed(self, event):
		self.textBox.config(state=NORMAL)
		self.onClick()

	def removeKeyboardFocus(self, event):
		self.textBox.config(state=DISABLED)

	def onClick(self):
		messageText = self.messageFilter(self.textBox.get("0.0", END)) #filter

		if "/shrug" in messageText :
			messageText =  "¯\_(ツ)_/¯"
			self.s.send(messageText)
		elif "/creep" in messageText :
			messageText = "( ͡° ͜ʖ ͡°)"
			self.s.send(messageText) #Just send the message
		elif "/smile" in messageText :
			messageText = "•ᴗ•"
			self.s.send(messageText) #Just send the message
		else:
			self.s.send(messageText.replace("\n", "")) #send over socket

		self.displayLocalMessage(messageText)
		self.chatBox.yview(END)
		self.textBox.delete("0.0", END)

	def quit(self):
		self.root.quit()

	def exit(self):
		self.s.close()
		sys.exit

	def messageFilter(self, messageText):
		EndFiltered = ''
		for i in range(len(messageText) - 1, -1, -1):
			if messageText[i] != '\n':
				EndFiltered = messageText[0:i + 1]
				break
		for i in range(0,len(EndFiltered), 1):
			if EndFiltered[i] != "\n":
				return EndFiltered[i:] + '\n'
		return ''

	def clean_msg(self, data):
		data = filter(lambda x: x in string.printable, data)
		data = data.replace("\t\t", "")
		data = re.sub(r'\[[0-9]*m', "", data)
		return data

	def displayLocalMessage(self, messageText):
		if messageText != '':
			self.chatBox.config(state=NORMAL)
			if self.chatBox.index('end') != None:
				#adds line of text to the end
				LineNumber = float(self.chatBox.index('end')) - 1.0
				#adds text to chatBox
				self.chatBox.insert(END, "YOU: " + messageText)
				#tkinter functions to customize aesthetics
				self.chatBox.tag_add("[YOU]", LineNumber, LineNumber + 0.4)
				self.chatBox.tag_config("[YOU]", foreground="#AA3939", font=("Courier", 12, "bold"), justify="right")
				self.chatBox.config(state=DISABLED)
		self.chatBox.yview(END)

	def displayRemoteMessage(self, data):
		print(data)
		messageText = self.messageFilter(data)
		if messageText != '':
			self.chatBox.config(state=NORMAL)
			if self.chatBox.index('end') != None:
				try:
					LineNumber = float(chatBox.index('end')) - 1.0
				except:
					LineNumber = 0.5
				self.chatBox.insert(END, "USER: " + messageText)
				self.chatBox.tag_add("USER", LineNumber, LineNumber + 0.6)
				self.chatBox.tag_config("USER", foreground="#6600ff", font=("Courier", 12, "bold"))
				self.chatBox.config(state=DISABLED)
		self.chatBox.yview(END)


	def ReceiveData(self):
		while 1:
			socket_list = [sys.stdin, self.s] 
			# Get the list sockets which are readable
			read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
			for sock in read_sockets:
				#incoming message from remote server
				if sock == self.s:
					data = self.s.recv(1025)
					if (REGISTRATION in data):
						reg = Registration(self.s, sock)
						while (not reg.ok):
							pass
						self.root.title(reg.username + " - MySlack V1.0")
					elif ("@BO" in data):
						self.s.send("-ok-")
					elif (KILL in data):
						self.s.close()
						sys.exit()
						break
					elif data != '':
						self.displayRemoteMessage(self.clean_msg(data))
					else:
						warning = "Server Is Offline"
						tk,MessageBox.showinfo("Error", warning)
						break
		self.s.close()
