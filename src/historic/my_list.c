#include "../server/server.h"

t_list		*create(void *data, t_list *next)
{
    t_list 	*new_node;

    new_node = (t_list*)malloc(sizeof(new_node));
    if (new_node == NULL)
        return NULL;
    new_node->data = data;
    new_node->next = next;
    return new_node;
}

t_list		*append(t_list *head, void *data)
{
    /* go to the last node */
    t_list *cursor;

    cursor = head;
    while (cursor->next != NULL)
        cursor = cursor->next;
    t_list *new_node = create(data, NULL);
    cursor->next = new_node;
    return (head);
}

void  		my_apply_on_list(t_list *begin, int (*f)())
{
  while (begin)
  {
    f(begin->data);
    begin = begin->next;
  }
}

void      display_list(t_list *node, t_serv_sock *s, int fd
          , void (*fptr)(void *, t_serv_sock *s, int))
{
  //avoid the empty data
    if (node != NULL)
      node = node->next;
    while (node != NULL)
    {
        (*fptr)(node->data, s, fd);
        node = node->next;
    }
}