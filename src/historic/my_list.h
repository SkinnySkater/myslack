#ifndef		_MY_LIST_H_
# define	_MY_LIST_H_

#include <stdlib.h>

#include "../server/server.h"

typedef struct     s_list
{
  void             *data;
  struct s_list    *next;
} t_list;

int			l_size(t_list *begin);

t_list		*create(void *data, t_list *next);

t_list		*append(t_list *head, void *data);

t_list 		*add_head(t_list *l, void *data);

t_list		*my_params_in_list(int argc, char **argv);

void  		my_apply_on_list(t_list *begin, int (*f)());

void		my_rev_list(t_list **begin);

t_list  	*my_find_node_elm_eq_in_list(t_list *begin
				     , void *data_ref
				     , int (*cmp)());

void      	display_list(t_list *node, t_serv_sock *s, int fd, void (*fptr)(void *, int));

#endif		/* !_MY_LIST_H_ */