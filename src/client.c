#include "client/client.h"

#include <stdio.h>  
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

void                   prompt()
{
    my_putstr("[YOU]: ");
}

int                    init_client_socket(int *sock, fd_set *master
    , fd_set *read_fds, int port_server, char* ip_server)
{
    struct sockaddr_in  addr_server;

    *sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (*sock < 0)
    {
        perror("socket() failed\n");
        return (1);
    }
    memset(&addr_server, 0, sizeof(addr_server));
    addr_server.sin_family = AF_INET;
    addr_server.sin_addr.s_addr = inet_addr(ip_server);
    addr_server.sin_port = htons((unsigned short)port_server);
    if (connect(*sock, (struct sockaddr *)&addr_server
        , sizeof(addr_server)) == -1)
    {
        perror("connect");
        return (1);
    }
    FD_ZERO(master);
    FD_ZERO(read_fds);
    FD_SET(0, master);
    FD_SET(*sock, master);
    return (0);
}

int                     treat_msg(int i, int sock, fd_set *read_fds)
{
    char                *buf = malloc(1024 + 1);
    int                 byte_recvd;

    if (i == 0)
        send_msg_serv(sock, buf);
    else
    {
        my_memset(buf, '\0', BUF_SIZE);
        byte_recvd = recv(sock, buf, sizeof(buf), 0);
        buf[byte_recvd] = '\0';
        if (check_srv_off(i, byte_recvd, read_fds))
            close(i);
        else if (login(&buf, sock))
            i = i;
        else if (!my_strcmp(buf, BACK_ONLIONE))
            write(sock, "[BackOnline]", 12);
        else if (!my_strcmp(buf, KILL))
        {
            close(sock);
            return (1);
        }
        else
            my_putstr(buf);
    }
    free(buf);
    return (0);
}


int                     run(int port, char *ip)
{
    int                 sock;
    int                 no_clients, i;
    fd_set              master;
    fd_set              read_fds;

    if (init_client_socket(&sock, &master, &read_fds, port, ip))
        return (1);
    no_clients = sock;
    my_putstr("\033[H\033[2J");

    while (TRUE)
    {
        read_fds = master;
        if (select(no_clients + 1, &read_fds, NULL, NULL, NULL) == -1)
            return perr("select");
        for (i = 0; i <= no_clients; i++)
        {
            if (FD_ISSET(i, &read_fds))
            {
                if (treat_msg(i, sock, &read_fds))
                    return (0);
            }
        }
    }
    close(sock);
}


int                 main(int argc, char **argv)
{
    int             portno;

    portno = -1;
    if (argc != 3)
        return print_usage(argv[0]);
    portno = check_port(argv[1]);
    if (portno < 0)
        return (-1);
    run(portno, argv[2]);
}
