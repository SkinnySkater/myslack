# MySlack

## SCREENSHOTS:
![alt text](screen/myslack.png "Python Gui client with C server")

## Getting Started
 You need Python 2.7 > or python 3

INSTAL TKINTER PACKAGE:
### Pyhton 3

```
apt-get install python3-tk 
```
### Pyhton 2.7
```
apt-get install python-tk 
```

## USER'S COMMANDS:
			--fallows by ENTER

				-Chat all:
					\A message

				-Chat current group:
					message

				-Private Chat:
					\P username

				-List User:
					\U

				-List Channels:
					\L

				-Change Groupe:
					\G groupe_name

				-Add New Group:
					\N

				-Leave Groupe:
					\X

				-Exit Server:
					\Q

				-Help:
					\H

				-Offline:
					\OFF

				-Change Name:
					\C new_name


## PROTOCOLS:

			REQUEST "uid#command#opts"

			-Subscription:
				»Client:"@new#username"
				«Server:"@reg"

			-Chat Current Group:
				»Client:"@chat#message"

			-Chat all:
				»Client:"@chat-all#message"

			-Private Chat:
				»Client:"@private#username#message"

			-List User:
				»Client:"@list_user"

			-List Channels:
				»Client:"@list_chan"

			-Change Groupe:
				»Client:"@G#groupe"

			-Leave Groupe:
				»Client:"@X"

			-Add Groupe:
				»Client:"@addG#groupe"

			-Exit Server:
				»Client:"@Q"

			-Display Help:
				»Client:"@H"

			-Add new group:
				»Client:"@N#groupe"

## Use it!
	How to use it ??? Just run:
		-generate the core files: make
		-The server first: ./server 
		-The clients ./client PORT HOST OR python python_client.py for graphical UI

## Authors
Myself Ovila Julien Lugard.

## License
Open Source - Free to use.