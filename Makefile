CC		=	gcc

CFLAGS	=	-Wall -Wextra -Werror  -W
LDFLAGS	=	-L. -lmy

HELP	=	src/util/my_strcmp.c src/util/my_putstr.c src/util/my_putchar.c\
			src/util/my_strlen.c src/util/my_getnbr.c src/util/is_nbr.c\
			src/util/my_put_nbr.c src/util/readline.c src/util/my_string.c\
			src/util/my_cls.c src/util/my_strcat.c src/util/my_memset.c\
			src/util/my_perror.c src/util/my_exit.c src/util/my_strtok.c src/historic/my_list.c\
			src/util/my_strcpy.c src/util/my_strdup.c src/util/my_isspace.c src/util/my_strstr.c
SRC		=	src/server2.c src/server/new_user.c src/server/queue.c src/server/message.c\
			src/server/groups.c src/server/group_modification.c src/server/handle_client.c src/server/bonus.c\
			src/server/historic.c src/server/protocol.c src/server/init_server.c src/server/server_usage.c
CSRC	= 	src/client.c src/client/client_func.c src/client/check_input.c

HOBJ	=	$(HELP:%.c=%.o)
OBJ		=	$(SRC:%.c=%.o)
COBJ	=	$(CSRC:%.c=%.o)

SERVER	=	server
CLIENT	=	client

.PHONY	:	fclean clean all re

all	:	$(SERVER) $(CLIENT)

$(SERVER)	:	$(OBJ) $(HOBJ)
	$(CC) $(SRC) $(HELP) -o $@
$(CLIENT)	:	$(COBJ) $(HOBJ)
	$(CC) $(CSRC) $(HELP) -o $@

clean	:
	$(RM) $(OBJ) $(HOBJ) $(COBJ) *.swp *~ t_* src/*.pyc
fclean	:	clean
	$(RM) $(CLIENT) $(SERVER)


re	:	fclean all